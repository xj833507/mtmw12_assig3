To run the code for this assignment, follow the steps:

1. Download this repository.
2. Ensure that the folder "plots" exists, otherwise you will not be able to
generate plots that will be output into the "plots" folder.
3. Run the command "python geostrophicWind.py" from your downloaded repository's
directory

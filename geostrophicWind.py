"""
MTMW12 assignment 3. ID:26833507 14 Oct 2019
Python3 code to numerically differentiate the pressure in order to
calculate the geostrophic wind relation using 2-point differencing
or 4-point differencing.

Code skeleton adopted from Hilary Weller
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from differentiate import *

def geostrophicWind(diff_method):
    """Function which calculates geostrophic wind using analytic gradient
    and the two-point differences method, before plotting and comparing them
    on a graph

    Parameters
    ----------
    diff_method : callable
        Numerical differentiation method for calculations

    Returns
    -------
    nothing

    """

    # Input parameters describing the problem
    import geoParameters as gp

    # Resolution
    N = 10
    dy = (gp.ymax - gp.ymin) / N # the length of the spacing

    # The spatial dimension, y:
    y = np.linspace(gp.ymin, gp.ymax, N+1)

    # The geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)

    # The pressure at the y points
    p = gp.pressure(y)

    # The pressure gradient and wind using two point differences
    dpdy = diff_method(p, dy)
    u_2point = gp.geoWind(dpdy)

    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    dot_size = 8

    if diff_method == gradient_2point:
        method_label = "Two-point differences"
        method_filename = "2"
    elif diff_method == gradient_4point:
        method_label = "Four-point differences"
        method_filename = "4"

    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, '-k', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label=method_label, \
              ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.plot(y[0]/1000, u_2point[0], '*r', \
              label="Backward/Forward Differences", \
              ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.plot(y[-1]/1000, u_2point[-1], '*r', \
              ms=12, markeredgewidth=1.5, markerfacecolor='none')
    if diff_method == gradient_4point:
        plt.plot(y[1]/1000, u_2point[1], '*', \
                  label="Two-point Differences", \
                  color="orange", \
                  ms=12, markeredgewidth=1.5, markerfacecolor='none')
        plt.plot(y[-2]/1000, u_2point[-2], '*', \
                  color="orange", \
                  ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best', prop={'size': 10})
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/{}point_GeoWindCent.pdf'.format(method_filename))
    plt.show()

    # Plot the absolute error between approximate and exact wind at y points
    plt.plot(y/1000, u_2point - uExact, '.-k', \
        label="Two-point Differences")
    plt.plot(y[0]/1000, u_2point[0] - uExact[0], '.r', markersize=dot_size, \
        label="Backward/Forward Differences")
    plt.plot(y[-1]/1000, u_2point[-1] - uExact[-1], '.r', markersize=dot_size)
    if diff_method == gradient_4point:
        plt.plot(y[1]/1000, u_2point[1] - uExact[1], '.', markersize=dot_size, \
            color="orange", \
            label="Backward/Forward Differences")
        plt.plot(y[-2]/1000, u_2point[-2] - uExact[-2], '.', \
            markersize=dot_size, \
            color="orange")
    plt.legend(loc='best', prop={'size': 10})
    plt.xlabel('y (km)')
    plt.ylabel(r'$\Delta$u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/{}point_GeoWindCent_Error.pdf'.format(method_filename))
    plt.show()

def geostrophicWind_ErrorExperiment(diff_method):
    """Function which carries out an experiment to test the error order
    of a certain numerical differentiation method.
    A plot of log10(error_abs_epsilon_list) against log10(dy_list) is generated.
    A linear line of best fit is also plotted on the graph.

    Parameters
    ----------
    diff_method : callable
        Numerical differentiation method for calculations

    Returns
    -------
    nothing

    """
    # Input parameters describing the problem
    import geoParameters as gp

    # List of resolutions to try
    N_list = [10,14,20,28,40,56,80,112,160]

    # Initialise array to store errors
    error_abs_epsilon_list = np.zeros_like(N_list, dtype=float)
    dy_list = np.zeros_like(N_list, dtype=float)

    for i in range(0,len(N_list)):
        dy = (gp.ymax - gp.ymin) / N_list[i] # the length of the spacing

        # The spatial dimension, y:
        y = np.linspace(gp.ymin, gp.ymax, N_list[i]+1)

        # The geostrophic wind calculated using the analytic gradient
        uExact = gp.uExact(y)

        # The pressure at the y points
        p = gp.pressure(y)

        # The pressure gradient and wind using two point differences
        dpdy = diff_method(p, dy)
        u_2point = gp.geoWind(dpdy)

        error_abs_epsilon_list[i] = \
            np.abs(u_2point[int(N_list[i]/2)] - uExact[int(N_list[i]/2)])
        dy_list[i] = dy

    # Create linear regression model to fit line on error plot

    model = stats.linregress( \
        [np.log10(dy_list), np.log10(error_abs_epsilon_list)])

    # Graph to compare the numerical and analytic solutions

    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    dot_size = 8
    ax = plt.gca()

    if diff_method == gradient_2point:
        method_label = "Two-point differences"
        method_filename = "2"
    elif diff_method == gradient_4point:
        method_label = "Four-point differences"
        method_filename = "4"

    # Plot the error experiment graph of log_10{error} against log_10{Delta y}
    plt.plot(np.log10(dy_list), np.log10(error_abs_epsilon_list), \
        '.k', label='Absolute Error', markersize=dot_size)
    plt.plot( \
        np.log10(dy_list), np.log10(dy_list) * model.slope + model.intercept, \
        '-b', label='Regression Line')
    plt.xlabel(r'$\log_{10}(\Delta y$ (m))')
    plt.ylabel(r'$\log_{10}(\varepsilon$ (m/s))')
    plt.text(.1, .9, \
    r'$\log_{{10}}(\varepsilon) = {0:8.2f} * \log_{{10}}(\Delta y) {1:+8.2f}$' \
        .format(model.slope, model.intercept), horizontalalignment='left', \
        verticalalignment='top', transform=ax.transAxes)
    plt.tight_layout()
    plt.savefig( \
        'plots/{}point_ErrorInvestigation.pdf'.format(method_filename))
    plt.show()

if __name__ == "__main__":

    # Question 1, 2-point difference
    geostrophicWind(gradient_2point)
    geostrophicWind_ErrorExperiment(gradient_2point)

    # Question 2, 4-point difference
    geostrophicWind(gradient_4point)
    geostrophicWind_ErrorExperiment(gradient_4point)

"""
MTMW12 assignment 3. ID:26833507 14 Oct 2019
This file contains functions that estimates a gradient of a mathematical
function using 2-point gradient method and 4-point gradient method

Code skeleton adopted from Hilary Weller
"""

import numpy as np

# Functions for calculating gradients

def gradient_2point(f, dx):
    """Estimate gradient using the second order finite difference method
    Forward and backward differences are used at the end-points

    Parameters
    ----------
    f : array of floats
        array containing values of the function calculated at intervals of dx
    dx : float
        interval (of independent variable) at which gradient is estimated

    Returns
    -------
    array of floats
        Array of estimated gradients for each x separated by interval dx

    """

    # Error checks
    if dx < 0:
        raise ValueError('Argument dx should not be negative')

    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f, dtype=float)

    # Two point differences at the end points
    dfdx[0] = (f[1] - f[0]) / dx
    dfdx[-1] = (f[-1] - f[-2]) / dx

    # Centred differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1]) / (2*dx)

    return dfdx

def gradient_4point(f, dx):
    """Estimate gradient using the second order finite difference method
    Forward and backward differences are used at the end-points

    Parameters
    ----------
    f : array of floats
        array containing values of the function calculated at intervals of dx
    dx : float
        interval (of independent variable) at which gradient is estimated

    Returns
    -------
    array of floats
        Array of estimated gradients for each x separated by interval dx

    """

    # Error checks
    if dx < 0:
        raise ValueError('Argument dx should not be negative')
    if len(f) < 5:
        raise ValueError('Not enough points to use this method')

    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f, dtype=float)

    # Backward and forward differences at the end points
    dfdx[0] = (f[1] - f[0]) / dx
    dfdx[-1] = (f[-1] - f[-2]) / dx

    # 2-point difference at the second to end points
    dfdx[1] = (f[2] - f[0]) / (2*dx)
    dfdx[-2] = (f[-1] - f[-3]) / (2*dx)

    # Centred differences for the mid-points
    for i in range(2,len(f)-2):
        dfdx[i] = (-f[i+2] + 8*f[i+1] - 8*f[i-1] + f[i-2]) / (12*dx)

    return dfdx

# Assertion tests for module

assert (gradient_2point([0,0,0,0,0], 1.0) == [0,0,0,0,0]).all(), \
"gradient_2point([0,0,0,0,0], 1.0) did not return the correct value"
assert (gradient_2point([9,5,3,5,6], 2.0) == [-2.0,-1.5,0.0,0.75,0.5]).all(), \
"gradient_2point([9,5,3,5,6], 2.0) did not return the correct value"

assert (gradient_4point([0,0,0,0,0], 1.0) == [0,0,0,0,0]).all(), \
"gradient_2point([0,0,0,0,0], 1.0) did not return the correct value"
assert (gradient_4point([9,5,3,5,6], 2.0) == \
[-2.0,-1.5,0.125,0.75,0.5]).all(), \
"gradient_2point([9,5,3,5,6], 2.0) did not return the correct value"

# Error catching tests for module

try:
    gradient_2point([0], -8)
except:
    pass
else:
    print("Error in gradient_2point, an exception should be raised if \
    input dx is negative.")

try:
    gradient_4point([0], -8)
except:
    pass
else:
    print("Error in gradient_4point, an exception should be raised if \
    input dx is negative.")

try:
    gradient_4point([0,1,2,3], 1.0)
except:
    pass
else:
    print("Error in gradient_4point, an exception should be raised if \
    input f has 4 elements or less.")

"""
MTMW12 assignment 3. ID:26833507 14 Oct 2019
File containing all input parameters and functions
for calculating geostropic wind

Code skeleton adopted from Hilary Weller
"""

import numpy as np

# Input parameters for calculating the geostrophic wind
# Import this file whereever you need these parameters and functions

pa = 1e5        # the mean pressure
pb = 200        # the magnitude of the pressure variations
f = 1e-4        # the Coriolis parameter
rho = 1.        # density
L = 2.4e6       # length scale of the pressure variations (k)
ymin = 0.0      # minimum space dimension
ymax = 1e6      # maximum space dimension (k)

def pressure(y):
    """Calculate pressure at a given location

    Parameters
    ----------
    y : float
        Vertical location

    Returns
    -------
    float
        pressure at given y, pa + pb * cos(y*pi / L)

    """

    # Error checks
    if all(this_y < ymin for this_y in y):
        raise ValueError('Argument y should not be smaller than ymin')
    if all(this_y > ymax for this_y in y):
        raise ValueError('Argument y should not be larger than ymax')

    return pa + pb * np.cos(y*np.pi / L)

def uExact(y):
    """Calculate the analytic geostrophic wind at given locations

    Parameters
    ----------
    y : float
        Vertical location

    Returns
    -------
    float
        Exact answer for velocity u = pb*pi / (rho*f*L) * sin(y*pi/L)

    """

    # Error checks
    if all(this_y < ymin for this_y in y):
        raise ValueError('Argument y should not be smaller than ymin')
    if all(this_y > ymax for this_y in y):
        raise ValueError('Argument y should not be larger than ymax')

    return pb*np.pi / (rho*f*L) * np.sin(y*np.pi/L)

def geoWind(dpdy):
    """Calculate the geostrophic wind as a function of pressure gradient"

    Parameters
    ----------
    dpdy : float
        Pressure gradient

    Returns
    -------
    float
        Estimate for velocity using pressure gradient u = -(dpdy) / (rho*f)

    """
    return -dpdy / (rho*f)


# Assertion tests for module

assert pressure(np.array([0])) == [100200], \
"pressure(0) did not return the correct value"
assert pressure(np.array([0.6e6])) == [1e5 + 100 * np.sqrt(2)], \
"pressure(0.6e6) did not return the correct value"

assert uExact(np.array([0])) == [0], \
"uExact(0) did not return the correct value"
assert uExact(np.array([0.6e6])) - \
5/12 * np.pi * np.sqrt(2) < [np.finfo(float).eps], \
"uExact(0.6e6) did not return the correct value"

assert geoWind(np.array([0])) == [0], \
"geoWind(0) did not return the correct value"
assert geoWind(np.array([20])) == [-200000], \
"geoWind(20) did not return the correct value"

# Error catching tests for module

try:
    pressure([2.5e6])
except:
    pass
else:
    print("Error in pressure, an exception should be raised if input y is \
    larger than ymax.")

try:
    pressure([-1])
except:
    pass
else:
    print("Error in pressure, an exception should be raised if input y is \
    smaller than ymin.")

try:
    uExact([2.5e6])
except:
    pass
else:
    print("Error in uExact, an exception should be raised if input y is \
    larger than ymax.")

try:
    uExact([-1])
except:
    pass
else:
    print("Error in uExact, an exception should be raised if input y is \
    smaller than ymin.")
